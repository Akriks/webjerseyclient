/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.universite.sav.Formulaires;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author akriks
 */
public class CommandeForm {
    UUID idClient;

    public UUID getIdClient() {
        return idClient;
    }

    public void setIdClient(UUID idClient) {
        this.idClient = idClient;
    }

    public List<Ligne> getLignes() {
        return lignes;
    }

    public void setLignes(List<Ligne> lignes) {
        this.lignes = lignes;
    }
    List<Ligne> lignes;
    
    public CommandeForm() {
        lignes=new ArrayList<Ligne>();
    }

    public void addLigne(Ligne ligne) {
        if (null==lignes) {
            lignes=new ArrayList<Ligne>();
        }
       lignes.add(ligne);
    }
}
