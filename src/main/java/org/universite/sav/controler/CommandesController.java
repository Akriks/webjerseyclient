package org.universite.sav.controler;

import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.universite.sav.Formulaires.CommandeForm;
import org.universite.sav.Formulaires.Ligne;
import org.universite.sav.api.ApiClient;
import org.universite.sav.api.ApiException;
import org.universite.sav.api.ApiResponse;
import org.universite.sav.api.GestionDesCommandesApi;
import org.universite.sav.api.model.Client;
import org.universite.sav.api.model.Commande;
import org.universite.sav.api.model.LigneCommande;
import org.universite.sav.api.model.Produit;

@Controller
public class CommandesController {

    private final   ApiClient apiClient = new ApiClient().setBasePath("http://localhost:8090/api");
    private final GestionDesCommandesApi apiCde = new GestionDesCommandesApi(apiClient);

    @RequestMapping(value = "/toto", method = RequestMethod.GET)
    public String toto(){
        return "toto";
    }
    // Renvoie vers la page avec le tableau contenant la liste des commandes
    @RequestMapping(value = "/commandes", method = RequestMethod.GET)
    public String listeCommmandes(Model model)  {
        try {
            model.addAttribute("listeCommandes", apiCde.getCommandes());
        } catch (ApiException ex) {
            Logger.getLogger(CommandesController.class.getName()).log(Level.SEVERE, null, ex);
            model.addAttribute("erreur", ex);
            return "error";
        }
        return "listeCdes";
    }

    // Initialise un objet Commande pour qu'il soit renseigné par le formulaire
    @RequestMapping(value = "/commandes/formCreation", method = RequestMethod.GET)
    public String afficheFormulaire(Map<String, Object> model) {
        CommandeForm commandeF = new CommandeForm();
        // max 5 lignes produits par commande
        for (int i = 1; i <= 5; i++) {
            commandeF.addLigne(new Ligne());
        }
        model.put("commandeF", commandeF);
        return "formCommande";
    }

    // Recoit les données du formulaire de création de commande
    // crée la création de commande  et renvoie vers le lien fournit par le link "self"
    // si présent sinon vers la liste des commandes
    // Noter l'usage du redirect
    @RequestMapping(value = "/commandes", method = RequestMethod.POST)
    public String creerCommande(CommandeForm commandeF, final RedirectAttributes redirectAttributes) {
        //On alimente la ressource commande avec les données du formulaire
        if ((null!=commandeF.getLignes()) && (commandeF.getLignes().size()>0)) {
            Commande commande=new Commande();
            commande.setClient(new Client().identifiant(commandeF.getIdClient()));
            commande.setDate(LocalDate.now());
            List<LigneCommande> lignes=
                commandeF.getLignes().stream().filter(ligne -> (ligne.getIdProduit()!=null)&&(ligne.getQuantite()>0))
                                              .map(ligneF -> { LigneCommande ligneCde=new LigneCommande();
                                                               ligneCde.setProduit(new Produit().identifiant(ligneF.getIdProduit()));
                                                               ligneCde.setQuantite(ligneF.getQuantite());
                                                               return ligneCde;
                                                              })
                                              .collect(Collectors.toList());
            commande.setLignes(lignes);
            try {
                // Invocation de  l'API de création de commande
                Commande cde=apiCde.creerCommande(commande);
                return "redirect:/details/"+cde.getIdentifiant();
            } catch (ApiException ex) {
                Logger.getLogger(CommandesController.class.getName()).log(Level.SEVERE, null, ex);
                redirectAttributes.addFlashAttribute("erreur",ex);
                return "redirect:/error";
            }
        }
        return "redirect:/commandes";
    }

    // Affiche le détail d'une commande
    @RequestMapping(value = "/details/{idCde}", method = RequestMethod.GET)
    public String afficheCommande(@PathVariable String idCde, Model model) {
        // Invoquer l'API REST pour rcuprer la commande
        try {

            // Pour récupérer les links qui se trouvent dans les entêtes HTTP, nous modifions l'appel
            //Commande cde = apiCde.getCommande(idCde);
             ApiResponse<Commande> response=apiCde.getCommandeWithHttpInfo(idCde);
             Commande cde =response.getData();
             List<String> linksStr=response.getHeaders().get("Link");
             if (null!=linksStr) {
                List<Link> links=linksStr.stream()
                                         .map(Link::valueOf)
                                         .collect(Collectors.toList());
                model.addAttribute("links",links);
   
                links.forEach(link -> {if (link.getRel().equalsIgnoreCase("delete")) {
                                              model.addAttribute("delete", true) ;
                                          }
                                          System.out.println(link.getRel());
                                       });
             }
             model.addAttribute("commande", cde);
             return "/detailCommande";  
        } catch (ApiException ex) {
             Logger.getLogger(CommandesController.class.getName()).log(Level.SEVERE, null, ex);
             model.addAttribute("erreur", ex);
             return "error";           
        }

    }

}
