package org.universite.sav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("org.universite.sav.controler")
@ComponentScan("org.universite.sav.conf")

public class SavWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SavWebApplication.class, args);
	}
}
