# WebJerseyClient

## Fonctionnel
Application de service après vente (SAV) sur la gestion des commandes.
Cette application permet de :
* consulter la liste des commandes
* consulter le détail d'une commande
* créer une nouvelle commande
* supprimer une commande

## Technique
Projet Springboot pour montrer l'utilisation de la spécification OPEN API coté client
* génération de code client Java Eclipse Jersey 
* appel des APIs REST du serveur créé lors des TPs précédents
* utilisation de Spring MVC pour l'IHM

Utilisation d'openapi-generator (https://openapi-generator.tech/) avec le plugin maven.

La génération java client avec le framework Eclipse Jersey est utilisé. La génération complète permet
de récupérer un pom avec toutes les dépendances nécessaires, ainsi qu'une classe de test montrant comment utiliser
le code client généré.


### HATEOAS : gestion des hyperliens
L'utilisation des liens permet d'afficher ou non le bouton de suppression des commandes
suivant que celle-ci est autorisée par le serveur ou non (transmission du lien avec rel=delete)



